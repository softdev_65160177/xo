/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.xoo;

/**
 *
 * @author bbnpo
 */
import java.util.*;

public class Xoo {
    static String[] board;
    static String turn;

    static String check() {
        String line = null;
        String[] winningLines = {
            "123", "456", "789", // แนวนอน
            "147", "258", "369", // แนวตั้ง
            "159", "357" // แนวทะแยง
        };

        for (String win : winningLines) {
            String[] parts = win.split("");
            line = board[Integer.parseInt(parts[0])-1] + board[Integer.parseInt(parts[1])-1] + board[Integer.parseInt(parts[2])-1];

            if (line.equals("XXX")) {
                return "X";
            } else if (line.equals("OOO")) {
                return "O";
            }
        }

        boolean isBoardFull = Arrays.stream(board).allMatch(s -> s.equals("X") || s.equals("O"));
        if (isBoardFull) {
            return "draw";
        }

        return null;
    }

    static void printTable() {
        System.out.println(board[0] + " " + board[1] + " " + board[2]);
        System.out.println(board[3] + " " + board[4] + " " + board[5]);
        System.out.println(board[6] + " " + board[7] + " " + board[8]);
    } 

    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        board = new String[9];
        turn = "O";
        String winner = null;

        for (int a = 0; a < 9; a++) {
            board[a] = String.valueOf(a + 1);
        }

        System.out.println("Welcome XO");
        printTable();

        while (winner == null) {
            System.out.println(turn + " turn");
            System.out.print("Please enter a number: ");

            int num = kb.nextInt();
            if (board[num - 1].equals(String.valueOf(num))) {
                board[num - 1] = turn;
                printTable();
                winner = check();

                turn = (turn.equals("X")) ? "O" : "X";
            }
        }

        if (winner.equalsIgnoreCase("draw")) {
            System.out.println("It's a draw");
        } else {
            System.out.println(winner + " win");
        }
    }
}

